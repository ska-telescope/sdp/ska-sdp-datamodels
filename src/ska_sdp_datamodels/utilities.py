"""This module contains an encode function to convert xarray.Dataset based
objects into a msgpack bytes object. Note that the following xarray models
have been tested:
1. Visibility
2. GainTable
3. StaticMaskTable
4. FlagTable
5. PointingTable
6. Image
7. GridData
8. ConvolutionFunction
9. Configuration
"""

import msgpack
import msgpack_numpy
import numpy
import xarray
from astropy.coordinates import EarthLocation, SkyCoord
from astropy.time import Time
from astropy.units import Quantity

try:
    import pyarrow

    PYARROW_AVAILABLE = True
except ModuleNotFoundError:
    PYARROW_AVAILABLE = False


from ska_sdp_datamodels.configuration import Configuration
from ska_sdp_datamodels.science_data_model import ReceptorFrame


def _str_to_skycoord(pointingcentre: str) -> SkyCoord:
    """Converts pointingcentre/phasecentre attribute from string
    to AstroPy SkyCoord"""
    pointingcentre_list = pointingcentre.split(" ")

    return SkyCoord(
        ra=float(pointingcentre_list[0]),
        dec=float(pointingcentre_list[1]),
        unit="deg",
        frame="icrs",
    )


def _convert_mjd_datetime(mjd):
    """
    Converts MJD seconds to AstroPy datetime object
    """
    datetime = []
    for timestamp in mjd:
        ymsdms = Time(timestamp / 86400.0, format="mjd").ymdhms
        if int(str(ymsdms.second).partition(".")[0]) < 10:
            date = (
                f"{ymsdms.year}-{ymsdms.month:02}-{ymsdms.day:02}T"
                f"{ymsdms.hour:02}:{ymsdms.minute:02}:0{ymsdms.second:02.9f}"
            )
        else:
            date = (
                f"{ymsdms.year}-{ymsdms.month:02}-{ymsdms.day:02}T"
                f"{ymsdms.hour:02}:{ymsdms.minute:02}:{ymsdms.second:02.9f}"
            )
        datetime.append(numpy.datetime64(date))

    return numpy.array(datetime)


def _fix_configuration(encoded_dict: dict) -> xarray.Dataset:
    """Fixes the antenna configuration"""
    try:
        config_attrs = encoded_dict["attrs"]["configuration"]
        location = encoded_dict["attrs"]["configuration"]["attrs"]["location"]
    except KeyError:
        # Handles datamodels without a configuration attribute
        config_attrs = encoded_dict
        location = config_attrs["attrs"]["location"]

    config_name = config_attrs["attrs"]["name"]
    antenna_names = config_attrs["data_vars"]["names"]["data"]
    xyz = config_attrs["data_vars"]["xyz"]["data"]
    diameter = config_attrs["data_vars"]["diameter"]["data"]
    offset = config_attrs["data_vars"]["offset"]["data"]
    stations = config_attrs["data_vars"]["stations"]["data"]
    mount = config_attrs["data_vars"]["mount"]["data"]
    vp_type = config_attrs["data_vars"]["vp_type"]["data"]
    frame = config_attrs["attrs"]["frame"]

    # There are inconsistent location types in the various
    # datamodels, but we keep to just one
    location = EarthLocation(
        x=Quantity(location[0], "m"),
        y=Quantity(location[1], "m"),
        z=Quantity(location[2], "m"),
    )
    if "receptor_frame1" in encoded_dict["attrs"].keys():
        # Fix receptor_frame1 and receptor_frame2. Both receptors
        # will have the same frame. Also fix the configuration
        # receptor_frame.
        receptor_frame_attr = encoded_dict["attrs"]["receptor_frame1"]
        encoded_dict["attrs"]["receptor_frame1"] = encoded_dict["attrs"][
            "receptor_frame2"
        ] = ReceptorFrame(ReceptorFrame.from_dict(receptor_frame_attr))

    try:
        # Fix both the receptor_frame and configuration receptor_frame
        # attributes
        # Fix the receptor_frame attribute
        receptor_frame_attr = ReceptorFrame(
            ReceptorFrame.from_dict(encoded_dict["attrs"]["receptor_frame"])
        )
        encoded_dict["attrs"]["receptor_frame"] = receptor_frame_attr

        # Fix the configuration receptor_frame
        try:
            receptor_frame_config = ReceptorFrame(
                ReceptorFrame.from_dict(
                    config_attrs["attrs"]["receptor_frame"]
                )
            )
        except TypeError:
            # Handles datamodels without a configuration attribute
            receptor_frame_config = receptor_frame_attr
    except KeyError:
        # Fix the configuration receptor_frame attribute only
        receptor_frame_config = ReceptorFrame(
            ReceptorFrame.from_dict(config_attrs["attrs"]["receptor_frame"])
        )

    configuration = Configuration.constructor(
        name=config_name,
        location=location,
        names=antenna_names,
        xyz=xyz,
        mount=mount,
        frame=frame,
        receptor_frame=receptor_frame_config,
        diameter=diameter,
        offset=offset,
        stations=stations,
        vp_type=vp_type,
    )

    encoded_dict["attrs"]["configuration"] = configuration

    return encoded_dict


def _dataset_encoder(obj):
    """Custom encoder for datamodel specific code.

    Classes that can be encoded currently:
    * xarray.Dataset (Specifically tested with Visibility)
    * Most numpy arrays (excluding datetime, arrays)
    * ska_sdp_datamodels.science_data_model.ReceptorFrame
    * astropy.coordinates.SkyCoord
    * astropy.coordinates.EarthLocation
    * pyarrow.lib.Table (when available)
    """
    # Dataset to dict conversion:
    if isinstance(obj, xarray.Dataset):
        if "configuration" in obj.attrs and isinstance(
            obj.attrs["configuration"], Configuration
        ):
            # AstroPy won't allow for equating a Quantity
            # with dimension to non-NaN/Inf so lets set location
            # to empty string before we update its values
            location = numpy.copy(obj.attrs["configuration"].attrs["location"])
            obj.attrs["configuration"].attrs["location"] = ""
            obj.attrs["configuration"].attrs["location"] = (
                Quantity(location).value[0],
                Quantity(location).value[1],
                Quantity(location).value[2],
            )

        out_dict = obj.to_dict(data="array")

        # we need to remove the datetime key, as this cannot be encoded
        if "datetime" in out_dict["data_vars"]:
            del out_dict["data_vars"]["datetime"]

        return out_dict

    # A method to get the ReceptorFrame object to encode
    if isinstance(obj, ReceptorFrame):
        return obj.to_dict()

    # A method to get the SkyCoord to convert
    if isinstance(obj, SkyCoord):
        return obj.to_string()

    # A method to get the EarthLocation to convert
    if isinstance(obj, EarthLocation):
        return obj.value.tolist()

    # Convert the Table type to a list
    if PYARROW_AVAILABLE and isinstance(obj, pyarrow.lib.Table):
        return obj.to_pylist()

    # Default to attempting to convert assuming numpy data
    return msgpack_numpy.encode(obj)


def encode(dataset: xarray.Dataset) -> bytes:
    """Encode a Dataset object into a msgpack bytes."""
    return msgpack.packb(dataset, default=_dataset_encoder)


def decode(bytes_dataset: bytes) -> xarray.Dataset:
    """Decode a msgpack bytes to a Dataset object."""
    data_raw = msgpack.unpackb(bytes_dataset, object_hook=msgpack_numpy.decode)

    # Fix the attributes
    attributes = data_raw["attrs"].keys()
    if "discrete_offset" in attributes:
        # Convert the discrete offset attribute in a PointingTable from
        # list to array
        data_raw["attrs"]["discrete_offset"] = numpy.array(
            data_raw["attrs"]["discrete_offset"]
        )
    if "_projection" in attributes:
        # The types of the decoded projection and reference pixel do not
        # match the input so we need to make them match
        data_raw["attrs"]["_projection"] = tuple(
            data_raw["attrs"]["_projection"]
        )
        data_raw["attrs"]["refpixel"] = numpy.array(
            data_raw["attrs"]["refpixel"]
        )
    if "pointingcentre" in attributes:
        # Convert the pointingcentre or phasecentre attribute from string
        # to a SkyCoord
        data_raw["attrs"]["pointingcentre"] = _str_to_skycoord(
            data_raw["attrs"]["pointingcentre"]
        )

    if "phasecentre" in attributes:
        data_raw["attrs"]["phasecentre"] = _str_to_skycoord(
            data_raw["attrs"]["phasecentre"]
        )

    if "commanded_pointing" in attributes:
        data_raw["attrs"]["commanded_pointing"] = numpy.array(
            data_raw["attrs"]["commanded_pointing"]
        )

    if "configuration" in attributes or "frame" in attributes:
        # Fix the configuration
        data_raw = _fix_configuration(data_raw)

    if "time" in data_raw["coords"]:
        # Construct datetime variable from time
        time_dict = data_raw["coords"]["time"]
        datetime_arr = xarray.DataArray(
            data=_convert_mjd_datetime(time_dict["data"]),
            coords={"time": time_dict["data"]},
            dims=time_dict["dims"],
            name="datetime",
            attrs={"units": "s"},
        )

        return xarray.Dataset.from_dict(data_raw).assign(datetime=datetime_arr)

    return xarray.Dataset.from_dict(data_raw)
