# pylint: disable=invalid-name, too-many-locals

"""
Functions working with Image model.
"""

import collections

import h5py
import numpy
import xarray
from astropy.io import fits
from astropy.wcs import WCS

from ska_sdp_datamodels.image.image_model import Image
from ska_sdp_datamodels.science_data_model import PolarisationFrame


def convert_image_to_hdf(im: Image, f):
    """Convert an Image to an HDF file

    :param im: Image
    :param f: hdf group
    :return: group with im added
    """
    if not isinstance(im, xarray.Dataset):
        raise ValueError(f"im is not xarray dataset {im}")
    if im.attrs["data_model"] != "Image":
        raise ValueError(f"fim is not an Image: {im}")

    f.attrs["data_model"] = "Image"
    f["data"] = im["pixels"].data
    f.attrs["wcs"] = numpy.string_(im.image_acc.wcs.to_header_string())
    f.attrs["phasecentre_coords"] = im.image_acc.phasecentre.to_string()
    f.attrs["phasecentre_frame"] = im.image_acc.phasecentre.frame.name
    f.attrs["polarisation_frame"] = im.image_acc.polarisation_frame.type
    f.attrs["frequency"] = im.frequency

    return f


def convert_hdf_to_image(f):
    """Convert HDF root to an Image

    :param f: hdf group
    :return: Image
    """
    if "data_model" in f.attrs.keys() and f.attrs["data_model"] == "Image":
        polarisation_frame = PolarisationFrame(f.attrs["polarisation_frame"])
        wcs = WCS(f.attrs["wcs"])
        data = numpy.array(f["data"])
        im = Image.constructor(
            data=data, polarisation_frame=polarisation_frame, wcs=wcs
        )
        return im

    return None


def export_image_to_hdf5(im, filename):
    """Export an Image or list to HDF5 format

    :param im: Image
    :param filename: Name of HDF5 file
    :return: None
    """

    if not isinstance(im, collections.abc.Iterable):
        im = [im]
    with h5py.File(filename, "w") as f:
        if isinstance(im, list):
            f.attrs["number_data_models"] = len(im)
            for i, v in enumerate(im):
                vf = f.create_group(f"Image{i}")
                convert_image_to_hdf(v, vf)
        else:
            f.attrs["number_data_models"] = 1
            vf = f.create_group("Image0")
            convert_image_to_hdf(im, vf)
        f.flush()
        f.close()


def import_image_from_hdf5(filename):
    """Import Image(s) from HDF5 format

    :param filename: Name of HDF5 file
    :return: single image or list of images
    """

    with h5py.File(filename, "r") as f:
        nimlist = f.attrs["number_data_models"]
        imlist = [convert_hdf_to_image(f[f"Image{i}"]) for i in range(nimlist)]
        if nimlist == 1:
            return imlist[0]

        return imlist


def _polarisation_frame_from_wcs(wcs, shape) -> PolarisationFrame:
    """Convert wcs to polarisation_frame

    See FITS definition in Table 29 of
    https://fits.gsfc.nasa.gov/standard40/fits_standard40draft1.pdf
    or subsequent revision

        1 I Standard Stokes unpolarized
        2 Q Standard Stokes linear
        3 U Standard Stokes linear
        4 V Standard Stokes circular
        −1 RR Right-right circular
        −2 LL Left-left circular
        −3 RL Right-left cross-circular
        −4 LR Left-right cross-circular
        −5 XX X parallel linear
        −6 YY Y parallel linear
        −7 XY XY cross linear
        −8 YX YX cross linear

        stokesI [1]
        stokesIQUV [1,2,3,4]
        circular [-1,-2,-3,-4]
        linear [-5,-6,-7,-8]

    For example::
        pol_frame =
            polarisation_frame_from_wcs(im.image_acc.wcs,
                im["pixels"].data.shape)


    :param wcs: World Coordinate System
    :param shape: Shape corresponding to wcs
    :returns: Polarisation_Frame object
    """
    # The third axis should be stokes:

    polarisation_frame = None

    if len(shape) == 2:
        polarisation_frame = PolarisationFrame("stokesI")
    else:
        npol = shape[1]
        pol = wcs.sub(["stokes"]).wcs_pix2world(range(npol), 0)[0]
        pol = numpy.array(pol, dtype="int")
        for key, value in PolarisationFrame.fits_codes.items():
            keypol = numpy.array(value)
            if numpy.array_equal(pol, keypol):
                polarisation_frame = PolarisationFrame(key)
                return polarisation_frame
    if polarisation_frame is None:
        raise ValueError("Cannot determine polarisation code")

    return polarisation_frame


def import_image_from_fits(fitsfile: str, fixpol=True) -> Image:
    """Read an Image from fits

    :param fitsfile: FITS file in storage
    :return: Image
    """
    # pylint: disable=no-member
    hdulist = fits.open(fitsfile)
    data = hdulist[0].data
    header = hdulist[0].header
    try:
        bmaj = header.cards["BMAJ"].value
        bmin = header.cards["BMIN"].value
        bpa = header.cards["BPA"].value
        clean_beam = {"bmaj": bmaj, "bmin": bmin, "bpa": bpa}
    except KeyError:
        clean_beam = None

    wcs = WCS(fitsfile)
    hdulist.close()

    polarisation_frame = PolarisationFrame("stokesI")

    if len(data.shape) == 4:
        # Images are RA, DEC, STOKES, FREQ
        if (
            wcs.axis_type_names[3] == "STOKES"
            or wcs.axis_type_names[2] == "FREQ"
        ):
            wcs = wcs.swapaxes(2, 3)
            data = numpy.transpose(data, (1, 0, 2, 3))
        try:
            polarisation_frame = _polarisation_frame_from_wcs(wcs, data.shape)
            # FITS and APP polarisation conventions differ
            if fixpol:
                permute = polarisation_frame.fits_to_datamodels[
                    polarisation_frame.type
                ]
                newim_data = data.copy()
                for ip, p in enumerate(permute):
                    newim_data[:, p, ...] = data[:, ip, ...]
                data = newim_data

        except ValueError:
            polarisation_frame = PolarisationFrame("stokesIQUV")

    elif len(data.shape) == 2:
        ny, nx = data.shape
        data.reshape([1, 1, ny, nx])

    return Image.constructor(
        data=data,
        polarisation_frame=polarisation_frame,
        wcs=wcs,
        clean_beam=clean_beam,
    )
