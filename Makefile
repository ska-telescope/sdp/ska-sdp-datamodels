include .make/base.mk
include .make/python.mk
include .make/oci.mk

PROJECT_NAME = ska-sdp-datamodels
CHANGELOG_FILE = CHANGELOG.rst

# W503: line break before binary operator
PYTHON_SWITCHES_FOR_FLAKE8 = --ignore=W503

# tmp, skip these files until fixed
PYTHON_VARS_AFTER_PYTEST = --ignore=tests/test_xarray_coordinate_support.py

# ignore the following pylint errors
# R0917 = too-many-positional-arguments -> basically the same as too-many-arguments,
#	which is already ignored in many places
PYTHON_SWITCHES_FOR_PYLINT = --disable=R0917