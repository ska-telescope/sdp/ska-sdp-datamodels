Changelog
=========

Development
-----------

- Replaced the MD files with RST ones (Changelog, Readme)
  (`MR82 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/82>`__)

1.0.0
-----

- ska-ser-sphinx-theme for RTD documentation build
  (`MR81 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/81>`__)
- xarray allowed version “^2024.9, <2024.11” due to a breaking change in
  2024.12
  (`MR80 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/80>`__)
- Update for flags to be included in what exported from vis to ms
  (`MR78 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/78>`__)
- Bug fix for WCS of GridData created from single-channel image, add
  channel_bandwidth attribute to GridData
  (`MR77 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/77>`__)
- Update and add test functions for image.image_io_and_convert.py
  (`MR76 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/76>`__)
- Bug fix for configuration name and datetime in decoded xarray object
  (`MR75 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/75>`__)

.. _section-1:

0.3.3
-----

- Add import_image_from_fits in image/
  (`MR74 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/74>`__)
- Make “interval” and “residual” data_vars in PointingTable optional
  (`MR73 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/73>`__)

.. _section-2:

0.3.2
-----

- Bug fix for convert_pointingtable_to_hdf so that it correctly adds
  track_duration attribute
  (`MR72 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/72>`__)
- Bug fix in the decoded pointingcentre, phasecentre, and configuration
  (`MR67 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/67>`__)

.. _section-3:

0.3.1
-----

- Bug fix of export_visibility_to_ms for exporting multiple visibilities
  (`MR68 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/68>`__)

.. _section-4:

0.3.0
-----

- Update astropy to 6+ and update other dependencies too
  (`MR65 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/65>`__)
- Add supporting create_named_configuration to use SKA TelModel
  (`MR64 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/64>`__)

.. _section-5:

0.2.10
------

- Bug fix for convert_pointingtable_to_hdf method so that it includes
  previously missing variables
  (`MR61 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/61>`__)
- Make imaging_weight and uvw_lambda optional data_vars of the
  Visibility object, set via Visibility.visibility_acc
  (`MR62 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/62>`__)

.. _section-6:

0.2.9
-----

- Bug fix for supporting combine_by_coords with Visibility
  (`MR59 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/59>`__)

.. _section-7:

0.2.8
-----

- Add commanded pointings at reference pointing to PointingTable
  (`MR58 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/58>`__)

.. _section-8:

0.2.7
-----

- Add option for a description for existing coordinates and variables of
  the PointingTable to be added as an attribute in the HDF5 file
  (`MR55 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/55>`__)
- Add additional metadata to PointingTable
  (`MR53 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/53>`__)
- Add datamodel for frequency-dependent only static RFI mask
  (`MR52 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/52>`__)

.. _section-9:

0.2.6
-----

- Updated import and export functions for PointingTable
  (`MR50 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/50>`__)
- Add support for differentiating between different orderings of the
  Linear Polarisation vector
  (`MR49 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/49>`__)

.. _section-10:

0.2.5
-----

- Refractor pointing table And Add two JSON related functions
  (`MR46 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/46>`__)

.. _section-11:

0.2.4
-----

- Bug Fix For Creating Visibility From Measurement Set Files
  (`MR45 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/45>`__)

.. _section-12:

0.2.3
-----

- Updated dependencies and fixed bug in creating FlagTable
  (`MR41 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/41>`__)
- Bug Fix For Exporting MS file with Multiple Polarisations
  (`MR40 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/40>`__)

.. _section-13:

0.2.2
-----

- Add scan attributes into Visibility
  (`MR39 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/39>`__)
- Add MsgPack support for xarray.Dataset objects, upgrade xarray to
  latest version
  (`MR38 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/38>`__)
- Use importlib.resources to obtain resource filenames
  (`MR36 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/36>`__)
- Bug fix for export_visibility_to_ms
  (`MR35 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/35>`__)

.. _section-14:

0.2.1
-----

- Migrate Measurement set related functions
  (`MR33 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/33>`__)
- Updated SKA LOW station coordinates to revision 4 and config_create
  functions
  (`MR31 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/31>`__)

.. _section-15:

0.2.0
-----

- Add support for a wider varient of casa calibration tables, G, B, Df,
  K and Kcross
  (`MR30 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/30>`__)
- Add the support of dealing with weights of different diameter antenna
  (`MR27 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/27>`__)
- Add support for casa calibration tables with a row for each time and
  antenna combination
  (`MR26 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/26>`__)

.. _section-16:

0.1.3
-----

- Add functions to read GainTable from CASA table
  (`MR21 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/21>`__)
- add function export_skymodel_to_text, needed to add the option of
  calibration with DP3 in rascil
  (`MR23 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/23>`__)
- Update GainTable to read different receptor frames as inputs
  (`MR22 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/22>`__)
- get_direction_time_location and calculate_visibility_hourangles allow
  for user-defined time inputs
  (`MR20 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/20>`__)
- Bug fix in PointingTable.pointingtable_acc.nrec
  (`MR20 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/20>`__)
- Bug fix when simulating with more than 2045 frequency channels
  ((`MR12 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/12>`__))

.. _section-17:

0.1.2
-----

- Add copy functions for SkyComponent and SkyModel
  ((`MR18 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/18>`__))

.. _section-18:

0.1.1
-----

- From RASCIL, added, create_gaintable_from_visibility,
  create_pointingtable_from_visibility, create_griddata_from_image,
  create_convolutionfunction_from_image,
  create_flagtable_from_visibility
  (`MR17 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/17>`__)
- Added create_image from RASCIL
  (`MR15 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/15>`__)

.. _section-19:

0.1.0
-----

- Added create_visibility and various create_configuration functions
  from RASCIL
  (`MR13 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/13>`__)
- Move class methods of classes inheriting from Dataset into the
  accessor classes
  (`MR11 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/11>`__)
- Restructured the repository
  (`MR10 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/10>`__)
- Moved Image() into its own file and added unit tests for it
  (`MR8 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/8>`__)
- Documentation improvements and updates
  (`MR9 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/9>`__,
  `MR7 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/7>`__,
  `MR4 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/4>`__)
- Migrated data models from RASCIL
  (`MR3 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/3>`__)
