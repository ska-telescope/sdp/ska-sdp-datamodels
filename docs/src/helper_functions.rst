.. _helper_functions:

Data Model Helper Functions
============================

Xarray Coordinate Support
-------------------------

We have provided coordinate support functions for the WCS coordinate system used in
``Image``, ``GridData`` and ``ConvolutionFunction``.
See :py:mod:`ska_sdp_datamodels.xarray_coordinate_support`

Data Models IO Functions
-------------------------

Each data model directory comes with a set of functions that allow
read/write to/from storage and to convert between various formats. See :ref:`api`.


MsgPack Support
---------------

:py:mod:`ska_sdp_datamodels.utilities` module contains ``encode`` and ``decode`` functions
used for ``xarray.Dataset``-based data models. These functions use
`msgpack <https://msgpack.org/>`_ for data serialization.
