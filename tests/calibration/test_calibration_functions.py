# pylint:disable=duplicate-code
"""
Test calibration data model functions.
"""

import json
import tempfile

import h5py
import numpy

from ska_sdp_datamodels.calibration import (
    convert_json_to_pointingtable,
    convert_pointingtable_to_json,
    export_gaintable_to_hdf5,
    export_pointingtable_to_hdf5,
    import_gaintable_from_hdf5,
    import_pointingtable_from_hdf5,
)
from tests.utils import data_model_equals


def test_export_gaintable_to_hdf5(gain_table):
    """
    We read back the file written by export_gaintable_to_hdf5
    and get the data that we used to write the file.
    """
    with tempfile.TemporaryDirectory() as temp_dir:
        test_hdf = f"{temp_dir}/test_export_gain_table_to_hdf5.hdf5"

        # tested function
        export_gaintable_to_hdf5(gain_table, test_hdf)

        with h5py.File(test_hdf, "r") as result_file:
            assert result_file.attrs["number_data_models"] == 1

            result_gt = result_file["GainTable0"]
            assert (
                result_gt.attrs["receptor_frame1"]
                == gain_table.receptor_frame1.type
            )
            assert (
                result_gt.attrs["receptor_frame2"]
                == gain_table.receptor_frame2.type
            )
            assert (
                result_gt.attrs["phasecentre_coords"]
                == gain_table.phasecentre.to_string()
            )
            assert (
                result_gt.attrs["phasecentre_frame"]
                == gain_table.phasecentre.frame.name
            )
            assert result_gt.attrs["data_model"] == "GainTable"
            assert (result_gt["data_time"] == gain_table.time.data).all()
            assert (result_gt["data_gain"] == gain_table.gain.data).all()


def test_import_gaintable_from_hdf5(gain_table):
    """
    We import a previously written HDF5 file containing
    gain data and we get the data we originally exported.

    Note: this test assumes that export_gaintable_to_hdf5
    works correctly, which is tested above.
    """
    with tempfile.TemporaryDirectory() as temp_dir:
        test_hdf = f"{temp_dir}/test_export_gain_table_to_hdf5.hdf5"
        export_gaintable_to_hdf5(gain_table, test_hdf)

        result = import_gaintable_from_hdf5(test_hdf)

        data_model_equals(result, gain_table)


def test_export_pointingtable_to_hdf5(pointing_table):
    """
    We read back the file written by export_pointingtable_to_hdf5
    and get the data that we used to write the file.
    """
    with tempfile.TemporaryDirectory() as temp_dir:
        test_hdf = f"{temp_dir}/test_export_pointing_to_hdf5.hdf5"

        comment = {
            "time": "The time in MJD at the middle of all the scans",
            "frequency": "The central frequency if fitting to gains and "
            "frequency at the higher end of the band if fitting to "
            "visibilities",
            "weight": "The inverse square of the uncertainties in the "
            "pointing values",
        }
        # tested function
        export_pointingtable_to_hdf5(pointing_table, test_hdf, **comment)

        with h5py.File(test_hdf, "r") as result_file:
            assert result_file.attrs["number_data_models"] == 1
            result_pt = result_file["PointingTable0"]
            assert (
                result_pt.attrs["receptor_frame"]
                == pointing_table.receptor_frame.type
            )
            assert (
                result_pt.attrs["pointingcentre_coords"]
                == pointing_table.pointingcentre.to_string()
            )
            assert (
                result_pt.attrs["pointingcentre_frame"]
                == pointing_table.pointingcentre.frame.name
            )
            assert result_pt.attrs["data_model"] == "PointingTable"
            assert (
                result_pt["data_nominal"] == pointing_table.nominal.data
            ).all()
            assert (
                result_pt["data_frequency"] == pointing_table.frequency.data
            ).all()
            assert (
                result_pt["data_fitted_width"]
                == pointing_table.fitted_width.data
            ).all()
            assert result_pt["data_time"] == pointing_table.time.data

            assert (
                result_pt["data_time"].attrs["description"] == comment["time"]
            )

            assert (
                result_pt["data_frequency"].attrs["description"]
                == comment["frequency"]
            )

            assert (
                result_pt["data_weight"].attrs["description"]
                == comment["weight"]
            )
            assert result_pt.attrs["band_type"] == pointing_table.band_type
            assert result_pt.attrs["scan_mode"] == pointing_table.scan_mode
            assert (
                result_pt.attrs["track_duration"]
                == pointing_table.track_duration
            )
            assert numpy.all(
                result_pt.attrs["commanded_pointing"]
                == pointing_table.commanded_pointing
            )
            assert numpy.all(
                result_pt.attrs["discrete_offset"]
                == pointing_table.discrete_offset
            )

            # "residual" and "interval" made optional data_vars
            # the pointing_table fixture does not contain them
            assert "data_residual" not in result_pt
            assert "data_interval" not in result_pt


def test_export_pointingtable_partial(pointing_table_vis):
    """
    Test of export_pointingtable_to_hdf5
    when some datavars are not provided.
    """
    with tempfile.TemporaryDirectory() as temp_dir:
        test_hdf = f"{temp_dir}/test_export_pointing_partial.hdf5"

        # tested function
        export_pointingtable_to_hdf5(pointing_table_vis, test_hdf)

        with h5py.File(test_hdf, "r") as result_file:
            assert result_file.attrs["number_data_models"] == 1

            result_pt = result_file["PointingTable0"]
            assert (
                result_pt.attrs["pointingcentre_frame"]
                == pointing_table_vis.pointingcentre.frame.name
            )
            assert (
                result_pt["data_weight"] == pointing_table_vis.weight.data
            ).all()
            assert (
                result_pt["data_nominal"] == pointing_table_vis.nominal.data
            ).all()
            assert "data_fitted_width" not in result_pt
            assert "data_fitted_height" not in result_pt

            assert "band_type" not in result_pt.attrs
            assert "scan_mode" not in result_pt.attrs
            assert "track_duration" not in result_pt.attrs
            assert "discrete_offset" not in result_pt.attrs
            assert "commanded_pointing" not in result_pt.attrs


def test_import_pointingtable_from_hdf5(pointing_table):
    """
    We import a previously written HDF5 file containing
    pointing data and we get the data we originally
    exported.

    Note: this test assumes that export_pointingtable_to_hdf5
    works correctly, which is tested above.
    """
    with tempfile.TemporaryDirectory() as temp_dir:
        test_hdf = f"{temp_dir}/test_export_pointing_to_hdf5.hdf5"
        export_pointingtable_to_hdf5(pointing_table, test_hdf)

        result = import_pointingtable_from_hdf5(test_hdf)
        data_model_equals(result, pointing_table)


def test_import_pointingtable_partial(pointing_table_vis):
    """
    Test for import_pointingtable_from_hdf5
    Assume no optional datavars are given.
    """
    with tempfile.TemporaryDirectory() as temp_dir:
        test_hdf = f"{temp_dir}/test_export_pointing_partial.hdf5"
        export_pointingtable_to_hdf5(pointing_table_vis, test_hdf)

        result = import_pointingtable_from_hdf5(test_hdf)
        data_model_equals(result, pointing_table_vis)


def test_convert_pointingtable_to_json(pointing_table):
    """
    We convert a pointing table to a JSON string and
    then import it back into a pointing table.
    """
    pointingtable_json = convert_pointingtable_to_json(pointing_table)
    pointing_dict = json.loads(pointingtable_json)

    assert (
        pointing_dict["attrs"]["receptor_frame"]
        == pointing_table.receptor_frame.type
    )
    assert (
        pointing_dict["attrs"]["pointingcentre_coords"]
        == pointing_table.pointingcentre.to_string()
    )
    assert (
        pointing_dict["attrs"]["pointingcentre_frame"]
        == pointing_table.pointingcentre.frame.name
    )
    assert pointing_dict["attrs"]["data_model"] == "PointingTable"
    assert pointing_dict["attrs"]["band_type"] == pointing_table.band_type
    assert pointing_dict["attrs"]["scan_mode"] == pointing_table.scan_mode
    assert (
        pointing_dict["attrs"]["track_duration"]
        == pointing_table.track_duration
    )
    assert (
        pointing_dict["attrs"]["discrete_offset"]
        == pointing_table.discrete_offset
    ).all()
    assert (
        pointing_dict["attrs"]["commanded_pointing"]
        == pointing_table.commanded_pointing
    ).all()

    assert (
        pointing_dict["data_vars"]["nominal"] == pointing_table.nominal.data
    ).all()
    assert (
        pointing_dict["coords"]["frequency"] == pointing_table.frequency.data
    ).all()


def test_convert_json_to_pointingtable(pointing_table):
    """
    We convert a JSON string to a pointing table and
    then import it back into a pointing table.
    """
    pointingtable_json = convert_pointingtable_to_json(pointing_table)

    result = convert_json_to_pointingtable(pointingtable_json)
    data_model_equals(result, pointing_table)
