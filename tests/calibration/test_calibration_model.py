# pylint:disable=too-many-locals, duplicate-code
"""
Unit tests for the Calibration Models
"""

import numpy
import pytest
from astropy.time import Time

from ska_sdp_datamodels.calibration.calibration_model import GainTable
from ska_sdp_datamodels.science_data_model.polarisation_model import (
    ReceptorFrame,
)

RECEPTOR_FRAME = ReceptorFrame("stokesI")


def test_gain_table_constructor_coords(gain_table):
    """
    Constructor correctly generates coordinates
    """
    # pylint: disable=duplicate-code
    expected_coords_keys = [
        "time",
        "antenna",
        "frequency",
        "receptor1",
        "receptor2",
    ]
    result_coords = gain_table.coords

    assert sorted(result_coords.keys()) == sorted(expected_coords_keys)
    assert result_coords["time"] == 1
    assert result_coords["antenna"] == 0
    assert result_coords["frequency"] == 1
    assert result_coords["receptor1"] == "I"
    assert result_coords["receptor2"] == "I"


def test_gain_table_constructor_datavars(gain_table):
    """
    Constructor correctly generates data variables
    """
    result_data_vars = gain_table.data_vars
    assert len(result_data_vars) == 5
    assert (result_data_vars["gain"] == 1).all()
    assert (result_data_vars["weight"] == 1).all()
    assert (result_data_vars["residual"] == 1).all()
    assert result_data_vars["interval"] == 1
    assert (
        result_data_vars["datetime"]
        == Time(1 / 86400.0, format="mjd", scale="utc").datetime64
    )


def test_gain_table_constructor_attrs(
    gain_table, low_aa05_config, phase_centre
):
    """
    Constructor correctly generates attributes.
    """
    result_attrs = gain_table.attrs

    assert len(result_attrs) == 6
    assert result_attrs["data_model"] == "GainTable"
    assert result_attrs["receptor_frame1"] == RECEPTOR_FRAME
    assert result_attrs["receptor_frame2"] == RECEPTOR_FRAME
    assert result_attrs["phasecentre"] == phase_centre
    assert result_attrs["configuration"] == low_aa05_config
    assert result_attrs["jones_type"] == "T"


def test_gain_table_copy(gain_table):
    """
    Test deep-copying GainTable
    """
    original_gain = gain_table.gain.data
    new_gain_data = gain_table.copy(deep=True)
    new_gain_data["gain"].data[...] = 100.0

    assert (gain_table["gain"].data == original_gain).all()
    assert (new_gain_data["gain"].data == 100.0).all()


def test_gain_table_property_accessor(gain_table):
    """
    GainTable.gaintable_acc (xarray accessor) returns
    properties correctly.
    """
    accessor_object = gain_table.gaintable_acc
    assert accessor_object.ntimes == 1
    assert accessor_object.nants == 1
    assert accessor_object.nchan == 1
    assert accessor_object.nrec == 1
    assert accessor_object.receptor1 == "I"
    assert accessor_object.receptor2 == "I"


def test_qa_gain_table(gain_table):
    """
    QualityAssessment of object data values
    are derived correctly.
    """
    accessor_object = gain_table.gaintable_acc
    expected_data = {
        "shape": (1, 1, 1, 1, 1),
        "maxabs-amp": 1,
        "minabs-amp": 1,
        "rms-amp": 0,
        "medianabs-amp": 1,
        "maxabs-phase": 0,
        "minabs-phase": 0,
        "rms-phase": 0,
        "medianabs-phase": 0,
        "residual": 1,
    }

    result_qa = accessor_object.qa_gain_table(context="Test")

    assert result_qa.context == "Test"
    for key, value in expected_data.items():
        assert result_qa.data[key] == value, f"{key} mismatch"


def test_invalid_receptor_frame(gain_table):
    """
    Raise ValueError when wrong receptor frame input is given
    """
    result_data_vars = gain_table.data_vars
    result_attrs = gain_table.attrs
    result_coords = gain_table.coords

    with pytest.raises(ValueError):
        GainTable.constructor(
            result_data_vars["gain"],
            result_coords["time"],
            result_data_vars["interval"],
            result_data_vars["weight"],
            result_data_vars["residual"],
            result_coords["frequency"],
            (RECEPTOR_FRAME, ReceptorFrame("circuloid")),
            result_attrs["phasecentre"],
            result_attrs["configuration"],
        )


def test_pointing_table_constructor_coords(pointing_table):
    """
    Constructor correctly generates coordinates
    """
    expected_coords_keys = [
        "time",
        "antenna",
        "frequency",
        "receptor",
        "angle",
    ]
    result_coords = pointing_table.coords

    assert sorted(result_coords.keys()) == sorted(expected_coords_keys)
    assert result_coords["time"] == 1
    assert result_coords["antenna"] == 0
    assert result_coords["frequency"] == 1
    assert result_coords["receptor"] == "I"
    assert (result_coords["angle"] == ["cross-el", "el"]).all()


def test_pointing_table_constructor_datavars(pointing_table):
    """
    Constructor correctly generates data variables
    interval and residual are optional and not present on pointing_table
    expected_width, fitted_* are also optional, but present
    """

    result_data_vars = pointing_table.data_vars
    assert len(result_data_vars) == 9
    assert (result_data_vars["pointing"] == 1).all()
    assert (result_data_vars["nominal"] == 1).all()
    assert (result_data_vars["weight"] == 1).all()
    assert (result_data_vars["expected_width"] == 1).all()
    assert (result_data_vars["fitted_width"] == 1).all()
    assert (result_data_vars["fitted_width_std"] == 0).all()
    assert (result_data_vars["fitted_height"] == 1).all()
    assert (result_data_vars["fitted_height_std"] == 0).all()
    assert (
        result_data_vars["datetime"]
        == Time(1 / 86400.0, format="mjd", scale="utc").datetime64
    )
    assert "residual" not in result_data_vars
    assert "interval" not in result_data_vars


def test_pointing_table_constructor_datavars_partial(pointing_table_vis):
    """
    Constructor correctly generates data variables
    interval and residual are optional but present in pointing_table_vis
    expected_width, fitted_* are also optional, but not present
    """

    result_data_vars = pointing_table_vis.data_vars
    assert len(result_data_vars) == 6
    assert (result_data_vars["pointing"] == 0.0).all()
    assert (result_data_vars["nominal"].data[:, :, :, :, 0] < -3.0).all()
    assert (result_data_vars["nominal"].data[:, :, :, :, 1] > 1.4).all()
    assert (result_data_vars["weight"] == 1.0).all()
    assert (result_data_vars["residual"] == 0.0).all()
    assert (result_data_vars["interval"] == 30.0).all()
    assert len(result_data_vars["datetime"]) == 10


def test_pointing_table_constructor_attrs(
    pointing_table, low_aa05_config, phase_centre
):
    """
    Constructor correctly generates attributes.
    """
    result_attrs = pointing_table.attrs

    assert len(result_attrs) == 10
    assert result_attrs["data_model"] == "PointingTable"
    assert result_attrs["receptor_frame"] == RECEPTOR_FRAME
    assert result_attrs["pointing_frame"] == "xel-el"

    assert result_attrs["band_type"] == "Band 2"
    assert result_attrs["scan_mode"] == "5-point"
    assert result_attrs["track_duration"] == 20.0
    assert (
        result_attrs["discrete_offset"]
        == numpy.array([-1.0, -0.33, 0.0, 0.33, 1.0])
    ).all()
    assert (
        result_attrs["commanded_pointing"]
        == numpy.array([-1.88300476e-04, -1.77350231e-07])
    ).all()
    assert result_attrs["pointingcentre"] == phase_centre
    assert result_attrs["configuration"] == low_aa05_config


def test_pointing_table_copy(pointing_table):
    """
    Test deep copy of PointingTable
    """
    original_pointing = pointing_table.pointing.data
    new_pointing_data = pointing_table.copy(deep=True)
    new_pointing_data["pointing"].data[...] = 100.0

    assert (pointing_table["pointing"].data == original_pointing).all()
    assert (new_pointing_data["pointing"].data == 100.0).all()


def test_pointing_table_property_accessor(pointing_table):
    """
    PointingTable.pointingtable_acc (xarray accessor) returns
    properties correctly.
    """
    accessor_object = pointing_table.pointingtable_acc
    assert accessor_object.nants == 1
    assert accessor_object.nchan == 1
    assert accessor_object.nrec == 1


def test_qa_pointing_table(pointing_table):
    """
    QualityAssessment of object data values
    are derived correctly.
    """
    accessor_object = pointing_table.pointingtable_acc
    expected_data = {
        "shape": (1, 1, 1, 1, 2),
        "maxabs-amp": 1,
        "minabs-amp": 1,
        "rms-amp": 0,
        "medianabs-amp": 1,
        "maxabs-phase": 0,
        "minabs-phase": 0,
        "rms-phase": 0,
        "medianabs-phase": 0,
        "residual": None,
    }

    result_qa = accessor_object.qa_pointing_table(context="Test")

    assert result_qa.context == "Test"
    for key, value in expected_data.items():
        assert result_qa.data[key] == value, f"{key} mismatch"
