# pylint: disable=duplicate-code
"""
Tests for functions in vis_io_ms.py
The tests here assume that msv2 module works.
For tests specific to the module, refer to test_msv2.py
"""

import os
import shutil
import tempfile

import numpy
import pytest

from ska_sdp_datamodels.science_data_model import PolarisationFrame
from ska_sdp_datamodels.visibility import create_visibility
from ska_sdp_datamodels.visibility.vis_io_ms import (
    create_visibility_from_ms,
    export_visibility_to_ms,
)

TIMES = (numpy.pi / 43200.0) * numpy.arange(0.0, 300.0, 30.0)
FREQUENCY = numpy.linspace(0.8e8, 1.2e8, 5)
CHANNEL_BANDWIDTH = numpy.array([1e7, 1e7, 1e7, 1e7, 1e7])
POLARISATION_FRAME = PolarisationFrame("linear")

casacore = pytest.importorskip("casacore")


@pytest.fixture(scope="module", name="msfile")
def fixture_ms_name():
    """
    Test setup that includes generating a temporary MS file
    """
    test_path = tempfile.mkdtemp(prefix="test-ms-", suffix=".tmp")
    ms_file = os.path.join(test_path, "test.ms")

    yield ms_file

    shutil.rmtree(ms_file)


def test_export_multi_visibility_to_ms(low_aa05_config, phase_centre, msfile):
    """
    Test for export_visibility_to_ms function
    """
    bvis = []
    for i in range(5):
        vis = create_visibility(
            low_aa05_config,
            TIMES,
            FREQUENCY,
            source=f"CAS{i}",
            channel_bandwidth=CHANNEL_BANDWIDTH,
            phasecentre=phase_centre,
        )
        bvis.append(vis)

    if os.path.exists(msfile):
        # remove temp file if exists
        shutil.rmtree(msfile, ignore_errors=False)

    export_visibility_to_ms(msfile, bvis)

    # Generate MS file from these visibilities

    vis = create_visibility_from_ms(msfile)
    assert len(vis) == 5
    for valueid, value in enumerate(vis):
        # assert all times are written
        # Times are 10, frequencies are 5, polarisations are 4
        assert value.vis.data.shape[0] == 10
        assert value.vis.data.shape[-1] == 4
        assert value.visibility_acc.polarisation_frame.type == "linear"
        if valueid == 0:
            assert value.source == "CAS0"
        elif valueid == 4:
            assert value.source == "CAS4"
