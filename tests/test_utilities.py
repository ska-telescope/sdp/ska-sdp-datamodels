"""Test functions to test the msgpack encode/decode functions."""

import astropy
import xarray

from ska_sdp_datamodels.utilities import decode, encode


def test_encode_decode_visibility(visibility):
    """Test both encode and decode of Visibility object"""
    encoded = encode(visibility)
    assert isinstance(encoded, bytes)

    decoded = decode(encoded)
    assert isinstance(decoded, xarray.Dataset)

    # Check the datetime variable exist
    assert (decoded.datetime.data == visibility.datetime.data).all()

    # Check that the phase centre is indeed a SkyCoord and not
    # a string
    assert isinstance(
        decoded.attrs["phasecentre"],
        astropy.coordinates.sky_coordinate.SkyCoord,
    )
    assert decoded.attrs["phasecentre"].ra.deg == visibility.phasecentre.ra.deg
    assert (
        decoded.attrs["phasecentre"].dec.deg == visibility.phasecentre.dec.deg
    )

    # Check that the configuration receptor_frame fix works
    assert (
        decoded.attrs["_polarisation_frame"]
        == visibility.attrs["_polarisation_frame"]
    )
    assert (
        decoded.attrs["configuration"].receptor_frame.type
        == visibility.attrs["configuration"].receptor_frame.type
        == "linear"
    )
    assert (
        decoded.attrs["configuration"].receptor_frame.names
        == visibility.attrs["configuration"].receptor_frame.names
        == ["X", "Y"]
    )


def test_encode_decode_gaintable(gain_table):
    """Test both encode and decode of GainTable"""

    encoded = encode(gain_table)
    assert isinstance(encoded, bytes)

    decoded = decode(encoded)
    assert isinstance(decoded, xarray.Dataset)

    # Check the datetime variable exist
    assert (decoded.datetime.data == gain_table.datetime.data).all()

    # Check that the phase centre is indeed a SkyCoord and not
    # a string
    assert isinstance(
        decoded.attrs["phasecentre"],
        astropy.coordinates.sky_coordinate.SkyCoord,
    )
    assert decoded.attrs["phasecentre"].ra.deg == gain_table.phasecentre.ra.deg
    assert (
        decoded.attrs["phasecentre"].dec.deg == gain_table.phasecentre.dec.deg
    )

    # Check that the configuration receptor_frame fix works
    assert (
        decoded.attrs["receptor_frame1"].names
        == decoded.attrs["receptor_frame2"].names
        == gain_table.attrs["receptor_frame1"].names
        == ["I"]
    )

    assert (
        decoded.attrs["receptor_frame1"].type
        == decoded.attrs["receptor_frame2"].type
        == "stokesI"
    )

    assert (
        gain_table.attrs["configuration"].receptor_frame.type
        == decoded.attrs["configuration"].receptor_frame.type
        == "linear"
    )


def test_encode_decode_staticmasktable(staticmask_table):
    """Test both encode and decode of StaticMaskTable"""
    encoded = encode(staticmask_table)
    assert isinstance(encoded, bytes)

    decoded = decode(encoded)
    assert isinstance(decoded, xarray.Dataset)

    # Check the datetime variable exist
    assert (decoded.mask.data == staticmask_table.mask.data).all()
    assert (decoded.frequency.data == staticmask_table.frequency.data).all()
    assert (
        decoded.channel_bandwidth.data
        == staticmask_table.channel_bandwidth.data
    ).all()


def test_encode_decode_flagtable(flag_table):
    """Test both encode and decode of StaticMaskTable"""
    encoded = encode(flag_table)
    assert isinstance(encoded, bytes)

    decoded = decode(encoded)
    assert isinstance(decoded, xarray.Dataset)

    # Check the datetime variable exist
    assert (decoded.datetime.data == flag_table.datetime.data).all()

    # Check the configuration
    assert (
        tuple(decoded.attrs["configuration"].location.itrs.data.xyz.value)
        == flag_table.attrs["configuration"].location
    )

    assert (
        decoded.attrs["configuration"].receptor_frame.names
        == flag_table.attrs["configuration"].receptor_frame.names
        == ["X", "Y"]
    )
    assert (
        decoded.attrs["configuration"].receptor_frame.type
        == flag_table.attrs["configuration"].receptor_frame.type
        == "linear"
    )


def test_encode_decode_pointingtable(pointing_table):
    """Test both encode and decode of PointingTable"""
    encoded = encode(pointing_table)
    assert isinstance(encoded, bytes)

    decoded = decode(encoded)
    assert isinstance(decoded, xarray.Dataset)

    # Check the datetime variable exist
    assert (decoded.datetime.data == pointing_table.datetime.data).all()

    # Check that the phase centre is indeed a SkyCoord and not
    # a string
    assert isinstance(
        decoded.attrs["pointingcentre"],
        astropy.coordinates.sky_coordinate.SkyCoord,
    )
    assert (
        decoded.attrs["pointingcentre"].ra.deg
        == pointing_table.pointingcentre.ra.deg
    )
    assert (
        decoded.attrs["pointingcentre"].dec.deg
        == pointing_table.pointingcentre.dec.deg
    )
    assert (
        decoded.attrs["commanded_pointing"]
        == pointing_table.attrs["commanded_pointing"]
    ).all()

    assert (
        tuple(decoded.attrs["configuration"].location.itrs.data.xyz.value)
        == pointing_table.attrs["configuration"].location
    )

    # Check that the configuration receptor_frame fix works
    assert (
        decoded.attrs["receptor_frame"].type
        == pointing_table.attrs["receptor_frame"].type
        == "stokesI"
    )
    assert (
        decoded.attrs["configuration"].receptor_frame.type
        == pointing_table.attrs["configuration"].receptor_frame.type
        == "linear"
    )


def test_encode_decode_image(image):
    """Test both encode and decode of Image"""
    encoded = encode(image)
    assert isinstance(encoded, bytes)

    decoded = decode(encoded)
    assert isinstance(decoded, xarray.Dataset)

    assert (
        type(decoded.attrs["_projection"])
        is type(image.attrs["_projection"])
        is tuple
    )
    assert decoded.attrs["_projection"] == image.attrs["_projection"]
    assert (decoded.attrs["refpixel"] == image.attrs["refpixel"]).all()
    assert (
        decoded.attrs["channel_bandwidth"] == image.attrs["channel_bandwidth"]
    )
    assert decoded.attrs["ra"] == image.attrs["ra"]
    assert decoded.attrs["dec"] == image.attrs["dec"]


def test_encode_decode_griddata(grid_data):
    """Test both encode and decode of GridData"""
    encoded = encode(grid_data)
    assert isinstance(encoded, bytes)

    decoded = decode(encoded)
    assert isinstance(decoded, xarray.Dataset)

    assert (decoded.frequency.data == grid_data.frequency.data).all()

    assert (
        decoded.attrs["_polarisation_frame"]
        == grid_data.attrs["_polarisation_frame"]
    )


def test_encode_decode_convfunc(conv_func):
    """Test both encode and decode of ConvolutionFunction"""
    encoded = encode(conv_func)
    assert isinstance(encoded, bytes)

    decoded = decode(encoded)
    assert isinstance(decoded, xarray.Dataset)

    assert (decoded.frequency.data == conv_func.frequency.data).all()

    assert (
        decoded.attrs["_polarisation_frame"]
        == conv_func.attrs["_polarisation_frame"]
    )


def test_encode_decode_configuration(low_aa05_config):
    """Test both encode and decode of Configuration"""
    encoded = encode(low_aa05_config)
    assert isinstance(encoded, bytes)

    decoded = decode(encoded)
    assert isinstance(decoded, xarray.Dataset)

    assert (
        tuple(decoded.attrs["location"]) == low_aa05_config.attrs["location"]
    )

    assert (
        decoded.attrs["receptor_frame"].type
        == low_aa05_config.attrs["receptor_frame"].type
        == "linear"
    )

    assert (
        decoded.attrs["receptor_frame"].names
        == low_aa05_config.attrs["receptor_frame"].names
        == ["X", "Y"]
    )
