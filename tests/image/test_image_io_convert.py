# pylint disable=too-many-locals
"""
Unit tests for IO and convert functions of Image
"""

import os
import tempfile

import h5py
import numpy
import pytest
import xarray
from astropy.io import fits
from astropy.wcs import WCS

from ska_sdp_datamodels.image import (
    Image,
    create_image,
    export_image_to_hdf5,
    import_image_from_fits,
    import_image_from_hdf5,
)
from ska_sdp_datamodels.image.image_io_and_convert import (
    _polarisation_frame_from_wcs,
)
from ska_sdp_datamodels.science_data_model import PolarisationFrame
from tests.utils import data_model_equals

CLEAN_BEAM = {"bmaj": 0.1, "bmin": 0.1, "bpa": 0.1}

# pylint: disable=duplicate-code
WCS_HEADER = {
    "CTYPE1": "RA---SIN",
    "CTYPE2": "DEC--SIN",
    "CTYPE3": "STOKES",  # no units, so no CUNIT3
    "CTYPE4": "FREQ",
    "CUNIT1": "deg",
    "CUNIT2": "deg",
    "CUNIT4": "Hz",
    "CRPIX1": 120,  # CRPIX1-4 are reference pixels
    "CRPIX2": 120,
    "CRPIX3": 4,  # Invalid value
    "CRPIX4": 1,
    "CRVAL1": 40.0,  # RA in deg
    "CRVAL2": 0.0,  # DEC in deg
    "CRVAL3": -1,  # Stokes
    "CDELT1": -0.1,
    "CDELT2": 0.1,  # abs(CDELT2) = cellsize in deg
    "CDELT3": -1,  # delta between polarisation values, 4 is invalid
    "CDELT4": 10.0,  # delta between frequency values
}


@pytest.fixture(scope="module", name="clean_beam")
def clean_beam_fixture(request):
    """
    Clean beam fixture
    """
    params = request.param
    if_valid = params.get("valid", True)
    if if_valid:
        return CLEAN_BEAM
    return None


@pytest.fixture(scope="module", name="polarisation_frame")
def polarisation_frame_fixture(request):
    """
    PolarisationFrame fixture
    """
    params = request.param
    polarisation = params.get("pol", "stokesIQUV")
    return PolarisationFrame(polarisation)


@pytest.fixture(scope="module", name="img_stokes_clean_beam")
def image_stokes_clean_beam_fixture(
    clean_beam, phase_centre, polarisation_frame
):
    """
    Generate a simple image
    """
    image = create_image(
        npixel=256,
        cellsize=0.000015,
        phasecentre=phase_centre,
        frequency=1.0e8,
        polarisation_frame=polarisation_frame,
        clean_beam=clean_beam,
    )
    return image


def test_export_visibility_to_hdf5(image):
    """
    We read back the file written by export_image_to_hdf5
    and get the data that we used to write the file.
    """
    with tempfile.TemporaryDirectory() as temp_dir:
        test_hdf = f"{temp_dir}/test_export_image_to_hdf5.hdf5"

        # tested function
        export_image_to_hdf5(image, test_hdf)

        with h5py.File(test_hdf, "r") as result_file:
            assert result_file.attrs["number_data_models"] == 1

            result_img = result_file["Image0"]
            assert (
                result_img.attrs["phasecentre_coords"]
                == image.image_acc.phasecentre.to_string()
            )
            assert (
                result_img.attrs["polarisation_frame"]
                == image.image_acc.polarisation_frame.type
            )
            assert (
                result_img.attrs["phasecentre_frame"]
                == image.image_acc.phasecentre.frame.name
            )
            assert result_img.attrs["data_model"] == "Image"
            assert (result_img["data"] == image.pixels.data).all()


def test_import_visibility_from_hdf5(image):
    """
    We import a previously written HDF5 file containing
    image data and we get the data we originally
    exported.

    Note: this test assumes that export_image_to_hdf5
    works correctly, which is tested above.
    """
    with tempfile.TemporaryDirectory() as temp_dir:
        # GIVEN
        test_hdf = f"{temp_dir}/test_export_image_to_hdf5.hdf5"
        export_image_to_hdf5(image, test_hdf)

        # WHEN
        result = import_image_from_hdf5(test_hdf)

        # THEN
        data_model_equals(result, image)


@pytest.mark.parametrize(
    "clean_beam",
    [
        ({"valid": True}),
        ({"valid": False}),
    ],
    indirect=True,
)
@pytest.mark.parametrize(
    "polarisation_frame",
    [
        ({"pol": "stokesI"}),
        ({"pol": "stokesIV"}),
        ({"pol": "stokesIQUV"}),
    ],
    indirect=True,
)
def test_import_image_from_fits_with_clean_beam(img_stokes_clean_beam):
    """
    We import a previously written HDF5 file containing
    image data and we get the data we originally
    exported.

    Note: this test assumes that export_image_to_fits
    works correctly, which is tested above.
    stokesI: npol =2, stokesIQUV: npol = 4
    """
    with tempfile.TemporaryDirectory() as temp_dir:
        # GIVEN
        test_fits = f"{temp_dir}/test_export_image_to_fits.fits"
        img_stokes_clean_beam.image_acc.export_to_fits(test_fits)

        # WHEN
        result = import_image_from_fits(test_fits)

        # THEN
        data_model_equals(result, img_stokes_clean_beam)

        assert (
            result.attrs["clean_beam"]
            == img_stokes_clean_beam.attrs["clean_beam"]
        )


def _export_test_fits(img, fits_file: str = "imaging.fits"):
    """
    Write an image to fits. The codes are copied from "export_to_fits" method

    :param fits_file: Name of output FITS file in storage
    """
    header = img.image_acc.wcs.to_header()
    if img["pixels"].data.dtype == "complex":
        fits.writeto(
            filename=fits_file,
            data=numpy.real(img["pixels"].data),
            header=header,
            overwrite=True,
        )
    else:
        fits.writeto(
            filename=fits_file,
            data=img["pixels"].data,
            header=header,
            overwrite=True,
        )


def test_import_image_from_fits_wrong_pol():
    """
    Test for invalid polarisation frame in the FITS file
    """
    npol = 4
    data = numpy.ones((10, npol, 256, 512))
    # Set pol_frame as linear so as to guarantee npol =4
    pol_frame = PolarisationFrame("linear")
    # WCS_HEADER will generate pol=[-2. -3. -4. -5.] which is invalid
    wcs = WCS(header=WCS_HEADER, naxis=4)

    image = Image.constructor(data, pol_frame, wcs, clean_beam=CLEAN_BEAM)
    with tempfile.TemporaryDirectory() as temp_dir:
        # GIVEN
        test_fits = f"{temp_dir}/test_export_image_to_fits.fits"
        _export_test_fits(image, test_fits)

        result = import_image_from_fits(test_fits)

        # THEN
        data_model_equals(result, image)
        # Finally, check that the polarisation frame is stokesIQUV
        assert result.image_acc.polarisation_frame == PolarisationFrame(
            "stokesIQUV"
        )


def test_write_image_to_zarr(image):
    """
    Test to see if an image can be written to
    and read from a zarr file (via default Dataset methods)
    """
    pytest.importorskip("zarr")
    new_image = image.copy(deep=True)
    new_image["pixels"].data[...] = numpy.random.random(image["pixels"].shape)

    # We cannot save dicts to a netcdf file
    new_image.attrs["clean_beam"] = ""

    with tempfile.TemporaryDirectory() as temp_dir:
        store = os.path.expanduser(f"{temp_dir}/test_image_to_zarr.zarr")
        new_image.to_zarr(
            store=store,
            chunk_store=store,
            mode="w",
        )
        loaded_data = xarray.open_zarr(store, chunk_store=store)
        assert (
            loaded_data["pixels"].data.compute() == new_image["pixels"].data
        ).all()


@pytest.mark.parametrize(
    "npol, pol_type",
    [
        (2, "stokesI"),
    ],
)
def test_polarisation_frame_from_wcs_stokes_i(npol, pol_type):
    """
    Test determining the polarisation frame from a WCS
        npol = 2 for stokesI
    """
    wcs = WCS(naxis=npol)
    shape = (10, 10)
    result = _polarisation_frame_from_wcs(wcs, shape)
    assert result.type == pol_type


@pytest.mark.parametrize(
    "npol, stokes, cdelta, pol_type",
    [
        (4, 0, 1, "stokesIQUV"),
        (4, 0, -1, "circular"),
        (4, -4, -1, "linear"),
    ],
)
def test_polarisation_frame_from_wcs_stokes(npol, stokes, cdelta, pol_type):
    """Test determining the polarisation frame from a WCS
    npol = 2 for stokesI and npol = 4 for stokesIQUV, circular and linear.
    """
    wcs = WCS(naxis=npol)
    wcs.wcs.ctype = ["RA---TAN", "DEC--TAN", "STOKES", "FREQ"]
    wcs.wcs.crval = [1, 1, stokes, 1]
    wcs.wcs.cdelt = [1, 1, cdelta, 1]
    shape = (10, npol, 10, 10)
    result = _polarisation_frame_from_wcs(wcs, shape)
    assert result.type == pol_type


def test_polarisation_frame_from_wcs_invalid():
    """Test that an invalid WCS raises an error
    _polarisation_frame_from_wcs can only support:
     stokesI [1]
     stokesIQUV [1,2,3,4]
     circular [-1,-2,-3,-4]
     linear [-5,-6,-7,-8]

     If the shape is not 2 or 4, or the values of the stokes are not
     consistent with the above, a ValueError is raised.
    """
    wcs = WCS(naxis=4)
    wcs.wcs.ctype = ["RA---TAN", "DEC--TAN", "STOKES", "FREQ"]
    wcs.wcs.crval = [0, 0, 99, 0]
    shape = (100, 4, 100, 100)
    with pytest.raises(ValueError):
        _polarisation_frame_from_wcs(wcs, shape)
